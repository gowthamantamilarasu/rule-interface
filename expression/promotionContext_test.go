package expression

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	promoOne   = "promo_one"
	promoTwo   = "promo_two"
	productOne = "product_one"
	productTwo = "product_two"
)

func TestNewContext(t *testing.T) {
	exp := &PromotionContext{defaultPromotions: map[string]map[string]byte{}}
	act := NewPromotionContext()
	assert.Equal(t, exp, act)
}

func TestContext_AddNewDefaultPromo(t *testing.T) {
	ctx := NewPromotionContext()
	cartProds := map[string]byte{
		productOne: 0,
		productTwo: 0,
	}
	ctx.AddDefaultPromo(promoOne, map[string]byte{productOne: 0, productTwo: 0}, cartProds)
	ctx.AddDefaultPromo(promoTwo, map[string]byte{productOne: 0}, cartProds)

	exp := []*Promotion{
		{
			ID: promoOne,
			Products: []string{
				productOne,
				productTwo,
			},
		},
		{
			ID: promoTwo,
			Products: []string{
				productOne,
			},
		},
	}
	act := ctx.DefaultPromotions()
	assert.Equal(t, exp, act)
}

func TestContext_AddNewDefaultPromoWithoutProductInCart(t *testing.T) {
	ctx := NewPromotionContext()
	cartProds := map[string]byte{
		productOne: 0,
	}
	ctx.AddDefaultPromo(promoOne, map[string]byte{productOne: 0, productTwo: 0}, cartProds)
	ctx.AddDefaultPromo(promoTwo, map[string]byte{productOne: 0}, cartProds)

	exp := []*Promotion{
		{
			ID: promoOne,
			Products: []string{
				productOne,
			},
		},
		{
			ID: promoTwo,
			Products: []string{
				productOne,
			},
		},
	}
	act := ctx.DefaultPromotions()
	assert.Equal(t, exp, act)
}

func TestContext_AddDuplicateDefaultPromo(t *testing.T) {
	ctx := NewPromotionContext()
	cartProds := map[string]byte{
		productOne: 0,
		productTwo: 0,
	}
	ctx.AddDefaultPromo(promoOne, map[string]byte{productOne: 0, productTwo: 0}, cartProds)
	ctx.AddDefaultPromo(promoOne, map[string]byte{productOne: 0}, cartProds)
	ctx.AddDefaultPromo(promoTwo, map[string]byte{productOne: 0}, cartProds)
	ctx.AddDefaultPromo(promoTwo, map[string]byte{productOne: 0, productTwo: 0}, cartProds)

	exp := []*Promotion{
		{
			ID: promoOne,
			Products: []string{
				productOne,
				productTwo,
			},
		},
		{
			ID: promoTwo,
			Products: []string{
				productOne,
				productTwo,
			},
		},
	}
	act := ctx.DefaultPromotions()
	assert.Equal(t, exp, act)
}

func TestContext_RemoveDefaultPromoPartially(t *testing.T) {
	ctx := NewPromotionContext()

	cartProds := map[string]byte{
		productOne: 0,
		productTwo: 0,
	}

	ctx.AddDefaultPromo(promoOne, map[string]byte{productOne: 0, productTwo: 0}, cartProds)
	ctx.AddDefaultPromo(promoOne, map[string]byte{productOne: 0}, cartProds)
	ctx.AddDefaultPromo(promoTwo, map[string]byte{productOne: 0}, cartProds)
	ctx.AddDefaultPromo(promoTwo, map[string]byte{productOne: 0, productTwo: 0}, cartProds)

	ctx.RemoveDefaultPromo(promoOne, map[string]byte{productOne: 0})
	ctx.RemoveDefaultPromo(promoTwo, map[string]byte{productTwo: 0})

	exp := []*Promotion{
		{
			ID: promoOne,
			Products: []string{
				productTwo,
			},
		},
		{
			ID: promoTwo,
			Products: []string{
				productOne,
			},
		},
	}
	act := ctx.DefaultPromotions()
	assert.Equal(t, exp, act)
}

func TestContext_RemoveDefaultPromoFully(t *testing.T) {
	ctx := NewPromotionContext()

	cartProds := map[string]byte{
		productOne: 0,
		productTwo: 0,
	}

	ctx.AddDefaultPromo(promoOne, map[string]byte{productOne: 0, productTwo: 0}, cartProds)
	ctx.AddDefaultPromo(promoOne, map[string]byte{productOne: 0}, cartProds)
	ctx.AddDefaultPromo(promoTwo, map[string]byte{productOne: 0}, cartProds)
	ctx.AddDefaultPromo(promoTwo, map[string]byte{productOne: 0, productTwo: 0}, cartProds)

	ctx.RemoveDefaultPromo(promoOne, map[string]byte{productOne: 0, productTwo: 0})
	ctx.RemoveDefaultPromo(promoTwo, map[string]byte{productOne: 0})
	ctx.RemoveDefaultPromo(promoTwo, map[string]byte{productTwo: 0})

	exp := make([]*Promotion, 0)
	act := ctx.DefaultPromotions()
	assert.Equal(t, exp, act)
}

func TestContext_RemoveNonExistentDefaultPromo(t *testing.T) {

	// Just to validate that it doesn't crash on something that doesn't exist
	ctx := NewPromotionContext()

	ctx.RemoveDefaultPromo(promoOne, map[string]byte{productOne: 0, productTwo: 0})
	ctx.RemoveDefaultPromo(promoTwo, map[string]byte{productOne: 0})
	ctx.RemoveDefaultPromo(promoTwo, map[string]byte{productTwo: 0})

	exp := make([]*Promotion, 0)
	act := ctx.DefaultPromotions()
	assert.Equal(t, exp, act)
}
