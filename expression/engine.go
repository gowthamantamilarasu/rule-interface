package expression

import (
	"time"
)

// RuleContext rule context containing all context information to execute a rule
type RuleContext struct {
	CurrentExecutingRule      Rule
	CurrentExecutingContainer *Container
	CompatibilityContext      *CompatibilityContext
	PromotionContext          *PromotionContext
	Channel                   string
	ProcessContext            string
	Category                  string
	ExecTime                  time.Time
	Entities                  Entities
	Products                  map[string]map[string]byte
	Categories                map[string]map[string]byte
}

// Entities entities containing all the rule expression logic
type Entities struct {
	*RuleContext
	Customer       *Customer
	Product        *Product
	Cart           *Cart
	Catalog        *Catalog
	InstalledBase  *InstalledBase
	Serviceability *Serviceability
}

// Rule signature for a engine component to invoke the ruling. First the context is parsed, then the current executing
// container, then the process context, and finally the category.
type Rule interface {
	ID() string
	Targets() map[string]byte
	Execute(*RuleContext, *Container)
}

//RuleKnowledgeBase contains rules, projection data for rule execution
type RuleKnowledgeBase struct {
	Rules      []Rule
	Products   map[string]map[string]byte
	Categories map[string]map[string]byte
}
