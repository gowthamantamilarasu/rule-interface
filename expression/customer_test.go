package expression

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	testCustomerType        = "SOHO"
	testOtherCustomerType   = "OtherSOHO"
	testAccountType         = "CustomerAccountType"
	testOtherAccountType    = "OtherCustomerAccountType"
	testCustomerStatus      = "Active"
	testOtherCustomerStatus = "InActive"
)

func TestCustomer_IsType(t *testing.T) {
	customer := &Customer{ID: "customer001", CustomerType: "SOHO"}

	assert.True(t, customer.IsType(testCustomerType))
	assert.False(t, customer.IsType(testOtherCustomerType))
}

func TestCustomer_IsAccountType(t *testing.T) {
	account := &Account{ID: "acc001", Status: "active", AccountType: "CustomerAccountType"}
	customer := customer([]*Account{account})

	assert.True(t, customer.IsAccountType(testAccountType))
	assert.False(t, customer.IsAccountType(testOtherAccountType))
}

func TestCustomer_HasStatus(t *testing.T) {
	customer := &Customer{ID: "customer001", Status: "Active"}

	assert.True(t, customer.HasStatus(testCustomerStatus))
	assert.False(t, customer.HasStatus(testOtherCustomerStatus))
}

func TestCustomer_ContainsProductInCategoryId(t *testing.T) {
	products := []*Product{&Product{ID: "prod001", Category: "cat001", Contract: &Contract{}}}
	subscriptions := []*Subscription{&Subscription{ID: "sub001", SubscriptionType: "subType1", Address: &Address{}, Products: products}}
	account := &Account{ID: "acc001", Status: "active", AccountType: "CustomerAccountType", Subscriptions: subscriptions}
	customer := customer([]*Account{account})

	assert.True(t, customer.ContainsProductInCategoryId("cat001"))
	assert.False(t, customer.ContainsProductInCategoryId("cat002"))
}

func TestCustomer_HasProductInventory(t *testing.T) {
	products := []*Product{&Product{ID: "prod001", Category: "cat001", Contract: &Contract{}}}
	subscriptions := []*Subscription{&Subscription{ID: "sub001", SubscriptionType: "subType1", Address: &Address{}, Products: products}}
	account := &Account{ID: "acc001", Status: "active", AccountType: "CustomerAccountType", Subscriptions: subscriptions}
	customer := customer([]*Account{account})

	assert.True(t, customer.HasProductInventory("prod001"))
	assert.False(t, customer.HasProductInventory("prod002"))
}

func TestCustomer_HasNumberOfProductsInCategoryId(t *testing.T) {
	products := []*Product{&Product{ID: "prod001", Category: "cat001", Contract: &Contract{}}, &Product{ID: "prod002", Category: "cat001", Contract: &Contract{}}}
	subscriptions := []*Subscription{&Subscription{ID: "sub001", SubscriptionType: "subType1", Address: &Address{}, Products: products}}
	account := &Account{ID: "acc001", Status: "active", AccountType: "CustomerAccountType", Subscriptions: subscriptions}
	customer := customer([]*Account{account})

	assert.True(t, customer.HasNumberOfProductsInCategoryId("cat001", 2))
	assert.False(t, customer.HasNumberOfProductsInCategoryId("cat001", 1))
	assert.False(t, customer.HasNumberOfProductsInCategoryId("cat002", 2))
}

func customer(accounts []*Account) *Customer {
	ctx := &RuleContext{
		Channel:  "",
		ExecTime: time.Time{},
	}
	entities := &Entities{
		RuleContext: ctx,
	}
	return NewCustomer(entities, accounts)
}
