package expression

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCatalog_Data(t *testing.T) {
	var expRes = map[string]*Product{"pro001": &Product{nil, "1", "cat001", nil}}
	catalog := &Catalog{data: map[string]*Product{"pro001": &Product{nil, "1", "cat001", nil}}}

	assert.Equal(t, expRes, catalog.Data())
}
