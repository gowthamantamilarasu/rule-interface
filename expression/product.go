package expression

import (
	"time"
)

// Product object
type Product struct {
	*Entities
	ID       string
	Category string
	Contract *Contract
}

//Contract to represent product contract structure
type Contract struct {
	StartDate time.Time
	EndDate   time.Time
}

// AllowCombinations function denotes the permitted combination of catalog data
func (p *Product) AllowCombinations() {
	targets := p.CurrentExecutingRule.Targets()
	for prod1 := range targets {
		for prod2 := range targets {
			if prod1 == prod2 {
				continue
			}
			p.CompatibilityContext.AllowCombination(prod1, prod2, p.CurrentExecutingRule.ID())
		}
	}
}

// DisallowCombinations function denotes the not permitted combination of catalog data
func (p *Product) DisallowCombinations() {
	targets := p.CurrentExecutingRule.Targets()
	for prod1 := range targets {
		for prod2 := range targets {
			if prod1 == prod2 {
				continue
			}
			p.CompatibilityContext.DisallowCombination(prod1, prod2, p.CurrentExecutingRule.ID())
		}
	}
}

// AllowStandalone allows each target product individually, without the link towards another product.
func (p *Product) AllowStandalone() {
	targets := p.CurrentExecutingRule.Targets()
	for cartProduct := range targets {
		p.CompatibilityContext.AllowStandalone(cartProduct, p.CurrentExecutingRule.ID())
	}
}

// DisallowStandalone allows each target product individually, without the link towards another product.
func (p *Product) DisallowStandalone() {
	targets := p.CurrentExecutingRule.Targets()
	for cartProduct := range targets {
		p.CompatibilityContext.DisallowStandalone(cartProduct, p.CurrentExecutingRule.ID())
	}
}

// AddDefaultPromo accepts a variadic argument of promo IDs. It will add the promo on the target products.
func (p *Product) AddDefaultPromo(promoIDs ...string) {
	for _, promoID := range promoIDs {
		p.PromotionContext.AddDefaultPromo(promoID, cartItemToMap(p.CurrentExecutingContainer.Items), p.CurrentExecutingRule.Targets())
	}
}

// RemoveDefaultPromo accepts a variadic argument of promo IDs. It will remove the promo from the target products.
func (p *Product) RemoveDefaultPromo(promoIDs ...string) {
	for _, promoID := range promoIDs {
		p.PromotionContext.RemoveDefaultPromo(promoID, p.CurrentExecutingRule.Targets())
	}
}

func cartItemToMap(items []*CartItem) map[string]byte {
	ret := make(map[string]byte, len(items))
	for _, item := range items {
		ret[item.ID] = 0
	}
	return ret
}
