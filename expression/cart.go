package expression

//Cart to represent cart structure
type Cart struct {
	*Entities
	CurrentIteratingContainer *Container
	Containers                []*Container
}

// Container is an object which is linked to a category and process context. It can be referenced through an ID. It has
// an array of *CartItem.
type Container struct {
	ID             string
	Category       string
	ProcessContext string
	Items          []*CartItem
}

// CartItem represents an item in the shopping cart container
type CartItem struct {
	ID       string
	Category string
	Version  string
}

// NewCart constructs a new *Cart struct. Has the *RuleContext and *proto.Cart as parameter. The context can be used
// to refer back to the current executing rules & container, and the *proto.Cart contains all of the info necessary
// for some expressions to do their evaluating.
func NewCart(entities *Entities, containers []*Container) *Cart {
	return &Cart{Entities: entities, Containers: containers}
}

// OfferContainerIsOfCategory will check whether or not the current offer container is one of the provided category IDs.
func (c *Cart) OfferContainerIsOfCategory(categoryIDs ...string) bool {
	for _, category := range categoryIDs {
		if c.CurrentIteratingContainer.Category == category {
			return true
		}
	}
	return false
}

// OfferContainerIsOfProcessContext will check whether or not the current offer container is one of the provided process
// contexts.
func (c *Cart) OfferContainerIsOfProcessContext(processContexts ...string) bool {
	for _, pc := range processContexts {
		if c.CurrentIteratingContainer.ProcessContext == pc {
			return true
		}
	}
	return false
}

// OfferContainerContainsProductOffering will check whether or not the current offer container has the provided product
// ID in the container.
func (c *Cart) OfferContainerContainsProductOffering(productID string) bool {
	for _, p := range c.CurrentIteratingContainer.Items {
		if p.ID == productID {
			return true
		}
	}
	return false
}

// OfferContainerContainsProductOfferingInCategory will check if the current evaluating offer container has product
// offering of the specified category.
func (c *Cart) OfferContainerContainsProductOfferingInCategory(categoryID string) bool {
	categories := c.Categories
	for _, ci := range c.CurrentIteratingContainer.Items {
		if ci.Category == categoryID {
			return true
		} else if categories[categoryID] != nil && categories[categoryID][ci.Category] == 0 {
			return true
		}
	}
	return false
}

// OfferContainerContainsProductOfferingWithVersion will check if the the current evaluating offer container has a
// product offering of the provided ID that matches the exact version specified.
func (c *Cart) OfferContainerContainsProductOfferingWithVersion(productID string, versions ...string) bool {
	for _, ci := range c.CurrentIteratingContainer.Items {
		for _, version := range versions {
			if ci.ID == productID && ci.Version == version {
				return true
			}
		}
	}
	return false
}

// ContainsOfferContainerOfCategory will check if there is at least one offer container in the cart which is of the
// specified categories.
func (c *Cart) ContainsOfferContainerOfCategory(categoryIDs ...string) bool {
	for _, oc := range c.Containers {
		for _, ca := range categoryIDs {
			if oc.Category == ca {
				return true
			}
		}
	}
	return false
}

// ContainsProductOffering will check if the cart (at least one container) has the specified product ID.
func (c *Cart) ContainsProductOffering(productIDs ...string) bool {
	for _, oc := range c.Containers {
		for _, ci := range oc.Items {
			for _, p := range productIDs {
				if ci.ID == p {
					return true
				}
			}
		}
	}
	return false
}

// ContainsProductOfferingInCategory will check if the cart (at least one container) has a product from the specified
// category.
func (c *Cart) ContainsProductOfferingInCategory(categoryIDs ...string) bool {
	categories := c.Categories
	for _, oc := range c.Containers {
		for _, ci := range oc.Items {
			for _, cat := range categoryIDs {
				if ci.Category == cat {
					return true
				}
				if categories[cat] != nil && categories[cat][ci.Category] == 0 {
					return true
				}
			}
		}
	}
	return false
}

// ContainsProductOfferingWithVersion will check if the cart has a product offering of the provided ID that matches the
// exact version specified. It is possible to add multiple versions in the expression, limiting to 1 SKU only.
func (c *Cart) ContainsProductOfferingWithVersion(productID string, versions ...string) bool {
	for _, oc := range c.Containers {
		for _, ci := range oc.Items {
			for _, version := range versions {
				if ci.ID == productID && ci.Version == version {
					return true
				}
			}
		}
	}
	return false
}
