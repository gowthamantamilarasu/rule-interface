package expression

// Catalog object
type Catalog struct {
	*Entities
	data map[string]*Product
}

// Data function
func (c *Catalog) Data() map[string]*Product {
	return c.data
}
