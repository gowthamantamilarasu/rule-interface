package expression

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestServiceability_HasAvailability(t *testing.T) {
	ctx := context()
	availabilities := make(map[string]string, 0)
	availabilities["DSL"] = "Available"
	availabilities["FIXED"] = "Not_Available"
	serviceability := NewServiceability(&ctx.Entities, availabilities, nil)
	assert.True(t, serviceability.HasAvailability("DSL", "Available"))
	assert.False(t, serviceability.HasAvailability("FIXED", "Available"))
}

func TestServiceability_HasServiceabilityItems(t *testing.T) {
	ctx := context()
	ServiceAbilityItems := make(map[string]map[string]map[string]byte, 0)
	ServiceAbilityItems["Fixed"] = map[string]map[string]byte{
		"maxspeed": {
			"100mbps": 0, "200mbps": 0, "300mbps": 0,
		},
	}
	serviceability := NewServiceability(&ctx.Entities, nil, ServiceAbilityItems)
	assert.True(t, serviceability.HasItems("Fixed", "maxspeed", "100mbps", "600mbps", "300mbps"))
	assert.False(t, serviceability.HasItems("Fixed", "maxspeed", "400mbps", "600mbps", "500mbps"))
}
